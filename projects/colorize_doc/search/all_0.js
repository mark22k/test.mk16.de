var searchData=
[
  ['colorize',['colorize',['../colorize_8cpp.html#a61e165812288726d761df38bdae5c113',1,'colorize(std::ostream &amp;os, colorize_color color, colorize_color background):&#160;colorize.cpp'],['../colorize_8hpp.html#a2bd094931902d1bf801eb78610cadbd4',1,'colorize(std::ostream &amp;, colorize_color=COLORIZE_NONE, colorize_color=COLORIZE_NONE):&#160;colorize.cpp']]],
  ['colorize_2ecpp',['colorize.cpp',['../colorize_8cpp.html',1,'']]],
  ['colorize_2ehpp',['colorize.hpp',['../colorize_8hpp.html',1,'']]],
  ['colorize_5fblack',['COLORIZE_BLACK',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476dfa2424332b984940eae87a14c83a95f932',1,'colorize.hpp']]],
  ['colorize_5fblue',['COLORIZE_BLUE',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476dfac373d2d872abdd4faca5d7778bdb2776',1,'colorize.hpp']]],
  ['colorize_5fcolor',['colorize_color',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476df',1,'colorize.hpp']]],
  ['colorize_5fcyan',['COLORIZE_CYAN',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476dfabf0314b173766af7166afeb3d709d8c2',1,'colorize.hpp']]],
  ['colorize_5fgreen',['COLORIZE_GREEN',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476dfa10a25f146d0cd6b3acb3de47c6084422',1,'colorize.hpp']]],
  ['colorize_5fmagenta',['COLORIZE_MAGENTA',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476dfab4facc10556e44dfd463baf91a5f2fc8',1,'colorize.hpp']]],
  ['colorize_5fnone',['COLORIZE_NONE',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476dfa795b165492a2a25b7adefda0e866641c',1,'colorize.hpp']]],
  ['colorize_5fred',['COLORIZE_RED',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476dfa04e2c870abb2c6186cd49776015fca99',1,'colorize.hpp']]],
  ['colorize_5fwhite',['COLORIZE_WHITE',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476dfa2a9046e419d28d5a2821e04dd731334f',1,'colorize.hpp']]],
  ['colorize_5fyellow',['COLORIZE_YELLOW',['../colorize_8hpp.html#ac4e1c6183a44d49585e48ae876d476dfad040b3bc7e8f04c2f9960d3ff65503e2',1,'colorize.hpp']]]
];
