var classmbignum =
[
    [ "mbignum", "classmbignum.html#a96eea0e2ecddc20dd6f12bd40dff0def", null ],
    [ "bad", "classmbignum.html#a39a9eca9246b4eccd41abf852342e9a9", null ],
    [ "destroy", "classmbignum.html#ab4e33cbea22cbdf4469b872aaf0ec5d8", null ],
    [ "good", "classmbignum.html#a480bafb7fed235cf232eb20ac846e3c7", null ],
    [ "length", "classmbignum.html#ae767c5e4c4aa95ebfbc4b8bf58675d72", null ],
    [ "maxlength", "classmbignum.html#a9554b8f96b4bd531107843c5983effb9", null ],
    [ "operator!=", "classmbignum.html#a583152b3378c4f629d52ad4a5b43d120", null ],
    [ "operator+", "classmbignum.html#a998d940687cce623cd1f570db1a1de98", null ],
    [ "operator+=", "classmbignum.html#aaad94adb382facaed8714fdaa95e75b2", null ],
    [ "operator-", "classmbignum.html#a7b2934109d52e0e684feedf3c0c22224", null ],
    [ "operator-=", "classmbignum.html#aa60fe87d1982ac4f5216df7edaf29c97", null ],
    [ "operator=", "classmbignum.html#a02720007606b23b81cab34afff1958a2", null ],
    [ "operator==", "classmbignum.html#acecfee221eab5e92c21f8c8748661b62", null ],
    [ "ptr", "classmbignum.html#a40f9fbc211b0674dab6294962a125a03", null ],
    [ "zero", "classmbignum.html#ae5ef4d1b83e0a1f4134815ecce77a9e7", null ],
    [ "to_string", "classmbignum.html#ae64c65d2dbd74e2c4490ebb74d73461a", null ],
    [ "nlen", "classmbignum.html#a02d7fbba88c5cce9c8bc736ee64f36bf", null ],
    [ "str", "classmbignum.html#a239060ae31a93a006b0c4b01049f0ba1", null ]
];