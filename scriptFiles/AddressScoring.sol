// SPDX-License-Identifier: WTFPL

pragma solidity ^0.8.7;

contract AddressScoring {
    
    struct Entry {
        uint value;
        bool exist;
    }
    
    struct AddressTrust {
        mapping(address => Entry) entries;
        uint8[] scores;
    }
    
    mapping(address => AddressTrust) internal trustForAddresses;
    
    function getTrust(address addr) public view returns(uint8) {
        uint sum = 0;
        for (uint i = 0; i < trustForAddresses[addr].scores.length; i++) {
            sum += trustForAddresses[addr].scores[i];
        }
        sum /= trustForAddresses[addr].scores.length;
        return uint8(sum);
    }
    
    function setTrust(address addr, uint8 trust) public returns(uint) {
        if (trustForAddresses[addr].entries[msg.sender].exist) {
            uint index = trustForAddresses[addr].entries[msg.sender].value;
            trustForAddresses[addr].scores[index] = trust;
            return 1;
        } else {
            trustForAddresses[addr].entries[msg.sender].value = trustForAddresses[addr].scores.length;
            trustForAddresses[addr].scores.push(trust);
            trustForAddresses[addr].entries[msg.sender].exist = true;
            return 2;
        }
    }
    
}
																   