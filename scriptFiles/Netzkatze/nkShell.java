package Netzkatze;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.Scanner;

public class nkShell {

	protected nkSocket sock;
	
	public static Scanner scanner;
	
	public nkShell(nkSocket sock) {
		this.sock = sock;
		
		if(sock.isReady()) {
			System.out.println("Der Socket ist bereit!");
		} else {
			System.out.println("Der Socket ist nicht bereit!");
			System.out.println("Das Programm löst eine Ausnahme aus.");
			throw new RuntimeException("Not ready! Debug: isClosed=" + sock.isClosed() +
					" isConnected=" + sock.isConnected() +
					" isBound=" + sock.isBound());
		}
	}
	
	public void run() {
		while(true) {
			System.out.print("> ");
			try {
				String str = scanner.nextLine();
				int status = execute(str);
				if(status == 2) {
					return;
				} else if(status == 0) {
					System.out.println("Fehler beim Ausführen des Kommandos.");
				} else if(status == 1) {
					System.out.println(">> Der Befehl wurde erfolgreich ausgeführt.");
				} else {
					System.out.println("Ein unbekannter Statuscode wurde zurückgegeben.");
				}
			} catch (Exception e) {
				System.out.println("Fehler: " + e.getLocalizedMessage());
			}	
		}
	}
	
	public int execute(String cmd) {
		try {
			String[] splitedCmd = cmd.split(" ");
			switch(splitedCmd[0]) {
			case "gets":
				execute("read 1 line");
				break;
				
			case "read":
				long cnt;
				String unit;
				try {
					cnt = Long.parseLong(splitedCmd[1]);
					unit = splitedCmd[2];
					
					switch (unit) {
					case "char":
					case "chars":
					case "characters":
					case "character":
						
						if(cnt > 255) {
							System.err.println("Es können maximal 255 Characters gelesen werden.");
							System.err.println("Es werden also nur 255 Characters gelesen.");
						}
							
						char[] ca = new char[255];
						try {
							this.sock.getReader().read(ca, 0, (int) cnt);
						} catch (IOException e) {
							System.err.println("Ein-/Ausgabefehler: Es konnten keine Daten von dem Socket gelesen werden.");
							return 0;
						}
						System.out.println(ca);
						
						break;
					case "line":
					case "lines":
						for(int i = 0; i < cnt; i++) {
							try {
								System.out.println(this.sock.getReader().readLine());
							} catch (IOException e) {
								System.err.println("Ein-/Ausgabefehler: Es konnten keine Daten von dem Socket gelesen werden.");
							}
						}
						
						break;
					}
					
				} catch (NumberFormatException e) {
					System.err.println("Die eingegebene Nummer konnte nicht gelesen werden: " + e.getLocalizedMessage());
					return 0;
				}
				break;

			case "print":
				this.sock.getWriter().print(cmd.substring(cmd.indexOf(' ') + 1));
				this.sock.flushWriter();
				
				break;
			case "println":
			case "puts":
				this.sock.getWriter().println(cmd.substring(cmd.indexOf(' ') + 1));
				this.sock.flushWriter();
				
				break;
			case "close":
				return 2;
				
			case "flush":
				this.sock.flush();
			}
		
		} catch (Exception e) {
			System.err.println("Es ist ein unbekannter Fehler aufgetreten: " + e.getLocalizedMessage());
			return 0;
		}
		return 1;
	}
	
	public static void main(String[] args) {
		nkSocket sock = null;
		nkShell shell = null;
		scanner = new Scanner(new InputStreamReader(System.in));
		
		try {
			String host = null;
			Integer port = null;
			
			System.out.print("Host: ");
			host = scanner.next();
			
			System.out.print("Port: ");
			port = scanner.nextInt();
			
			System.out.println("Es wird ein Verbindung zum Host " + host + " und dem Port " + port + " aufgebaut.");
			scanner.nextLine();
			
			sock = new nkSocket(host, port);
			shell = new nkShell(sock);
			shell.run();
			
		} catch (UnknownHostException e) {
			System.err.println("Ein unbekannter Host wurde versucht, zu einer Internet Adresse aufzulösen: " + e.getLocalizedMessage());
		} catch (IOException e) {
			System.err.println("Ein-/Ausgabefehler: " + e.getLocalizedMessage());
		} catch (Exception e) {
			System.err.println("Fehler: " + e.getLocalizedMessage());
		}
		
		try {
			if(sock != null)
				sock.close();
		} catch (Exception e) {
			System.err.println("Fehler beim Schließen des Sockets: " + e.getLocalizedMessage());
		}
		
		System.out.println("Ende!");
	}
	
}
