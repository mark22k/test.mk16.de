package Netzkatze;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class nkSocket implements Closeable {

	protected Socket sock;
	protected InputStreamReader dis;
	protected OutputStreamWriter dos;
	protected BufferedReader reader;
	protected PrintWriter writer;
	
	public nkSocket(String host, Integer port) throws IOException, UnknownHostException {
		this.sock = new Socket(InetAddress.getByName(host), port);
		this.dis = new InputStreamReader(this.sock.getInputStream());
		this.dos = new OutputStreamWriter(this.sock.getOutputStream());
		this.writer = new PrintWriter(this.dos);
		this.reader = new BufferedReader(this.dis);
	}
	
	public BufferedReader getReader() {
		return this.reader;
	}
	
	public PrintWriter getWriter() {
		return this.writer;
	}
	
	public boolean isClosed() {
		return this.sock.isClosed();
	}
	
	public boolean isConnected() {
		return this.sock.isConnected();
	}
	
	public boolean isBound() {
		return this.sock.isBound();
	}
	
	public boolean isReady() {
		return (! this.sock.isClosed()) && this.sock.isConnected() && this.sock.isBound();
	}
	
	public void flushReader() throws IOException {
		dos.flush();
	}
	
	public void flushWriter() {
		writer.flush();
	}
	
	public void flush() throws IOException {
		flushReader();
		flushWriter();
	}
	
	@Override
	public void close() {
		/* sock.close();
		this.writer.close();
		this.reader.close();
		this.dis.close();
		this.dos.close(); */
		this.writer.close();
		
		try {
			this.reader.close();
		} catch (IOException e) {
			System.err.println("Der Reader konnte nicht geschlossen werden: " + e.getLocalizedMessage());
		}
		
		try {
			this.dis.close();
		} catch (IOException e) {
			System.err.println("Der Input Stream Reader konnte nicht geschlossen werden: " + e.getLocalizedMessage());
		}
		
		try {
			this.dos.close();
		} catch (IOException e) {
			System.err.println("Der Output Stream Reader konnte nicht geschlossen werden: " + e.getLocalizedMessage());
		}
		
		try {
			this.sock.close();
		} catch (IOException e) {
			System.err.println("Der Socket konnte nicht geschlossen werden: " + e.getLocalizedMessage());
		}
	}
	
}
