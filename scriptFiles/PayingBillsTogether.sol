// SPDX-License-Identifier: WTFPL
/*
    Copyright (c) 2021 Marek Küthe
    This work is free. You can redistribute it and/or modify it under the
    terms of the Do What The Fuck You Want To Public License, Version 2,
    as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
*/

pragma solidity ^0.8.7;

import "@openzeppelin/contracts/access/Ownable.sol";

/*
    Community projects often involve collecting donations. This contract makes it easy to
    keep an overview and to carry out a collection campaign.
    
    This contract collects donations for a specific "bill". You enter the amount of the
    invoice at "toPay". The minimum donation is entered under "minimalDonation". This can
    be useful if the donation unlocks a certain feature.
    
    The functions are explained below in the source code.
*/

contract PayingBillsTogether is Ownable {
    
    /* Event is triggered when a donation is received. */
    event ReceivedDonation();
    /* Event is triggered when the owner transfers money that has been collected through
        donations */
    event BillPaid(address addr, uint amount);
    /* Is triggered when the donation is repaid to an address. */
    event PayDonationsBack(uint amount);
    /* Is triggered when the contract is deactivated - so no longer accepts donations. */
    event ContractDeactivated();
    
    uint public minimalDonation = 1 * 10 ** 18;  /* Minimum amount to pay is 1 cTH */
    uint public toPay = 10 * 10 ** 18;  /* Amount to be payed is 10 cTH */
    
    /* The timeout function can be used to refund donors in case the donation goes wrong. */
    /* If after this time the necessary money is not collected, users are allowed to issue a refund. */
    uint public timeoutToCollect;
    /* If after this time nothing has been done with the money (e.g. because the thing for
        which the donation is no longer exists), the users are allowed to make a refund. This
        happens regardless of how much money has been raised. */
    uint public timeoutToPayBill;
    /* One or both functions can be deactivated by setting them to 0
        (this is done as a parameter in the constructor). */
    
    /* Internal variables that collect data about the donors. */
    /* The fact that the variables are marked as "internal" does not prevent them from 
        being read by third parties, as blockchain data is public. This is only intended
        to be an attempt to make reading more difficult. */
    mapping(address => string) internal emails;
    mapping(address => string) internal gpgkeys;
    mapping(address => uint) internal payedAmount;
    address [] internal doners;
    
    /* If no more donations are to be collected, the contract can be "switched off". */
    bool public contractEnabled;
    
    /* Constructor. The arguments are used to set the timeout function. */
    constructor(uint initialTimeoutToCollect, uint initialTimeoutToPayBill) {
        timeoutToCollect = initialTimeoutToCollect;
        timeoutToPayBill = initialTimeoutToPayBill;
        contractEnabled = true;
    }
    
    /* Returns true if the contract is still actively collecting donations, otherwise false. */
    function isEnabled() public view returns(bool) {
        return contractEnabled;
    }
    
    /* Deactivates the contract. This prevents the contract from accepting further donations. */
    function disableContract() public onlyOwner {
        emit ContractDeactivated();
        contractEnabled = false;
    }
    
    /* Allows the owner of the contract to pay the bill by transferring the collected
        donations to an account. */
    function payBill(address payable addr, uint amount) public onlyOwner {
        emit BillPaid(msg.sender, amount);
        addr.transfer(amount);
    }
    
    /* Allows the owner of the contract to see the addresses of the donors. This is
        necessary to get their email and GPGKey. */
    function getAddresses(uint index) public onlyOwner view returns(address) {
        return doners[index];
    }
    
    /* Allows the owner of the contract to see the amount each donated. */
    function getAmount(address addr) public onlyOwner view returns(uint) {
        return payedAmount[addr];
    }
    
    /* Returns their email address, if the donor specified one. The address of the
        donor is required as an argument. */
    function getEmail(address addr) public onlyOwner view returns(string memory) {
        return emails[addr];
    }
    
    /* Returns their gpg key, if the donor specified one. The address of the
        donor is required as an argument. */
    function getGpgkey(address addr) public onlyOwner view returns(string memory) {
        return gpgkeys[addr];
    }
    
    /* Returns the amount of all collected donations. */
    function remainingAmount() public view returns(uint) {
        return address(this).balance;
    }
    
    /* Returns the donation goal. */
    function moneyNeededForBill() public view returns(uint) {
        return toPay;
    }
    
    /* Returns the amount that is still needed to pay the invoice. */
    function moneyStillNeededForBill() public view returns(uint) {
        return toPay - address(this).balance;
    }
    
    /* Private function which is used to refund the donor their money. */
    function payDonationsBack() private returns(bool) {
        for(uint i; i < doners.length; i++) {
            emit PayDonationsBack(payedAmount[doners[i]]);
            payedAmount[doners[i]] = 0;
            payable(doners[i]).transfer(payedAmount[doners[i]]);
        }
        return true;
    }
    
    /* If the function is activated, users can reclaim donations
        for donors if the invoice has not been paid within a period
        of time. */
    function billTimeout() public returns(bool) {
        require(timeoutToPayBill != 0);
        require(timeoutToPayBill >= block.timestamp);
        return payDonationsBack();
    }
    
    /* If not enough money has been collected for the bill within
        a period of time, users can claim their money back here. This
        only works if this function has been activated. */
    function donationTimeout() public returns(bool) {
        require(timeoutToCollect != 0);
        require(timeoutToCollect >= block.timestamp);
        require(address(this).balance < toPay);
        return payDonationsBack();
    }
    
    /* If the donation fails for any reason, the owner can have the
        donations reimbursed to the donors here. */
    function donationFailed() public onlyOwner returns(bool) {
        return payDonationsBack();
    }
    
    /* Function that accepts donations. If the donor so wishes,
        they can leave their email address and a URL (or keyid) of their GPG key. */
    function sendDonation(string memory donersEmail, string memory donersGpgkey) external payable {
        require(contractEnabled);
        require(msg.value >= minimalDonation);
        
        emit ReceivedDonation();
        emails[msg.sender] = donersEmail;
        gpgkeys[msg.sender] = donersGpgkey;
        payedAmount[msg.sender] = msg.value;
        doners.push(msg.sender);
    }
    
}
