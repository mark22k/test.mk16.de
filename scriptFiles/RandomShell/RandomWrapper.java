
import java.security.SecureRandom;
import java.util.Stack;

public class RandomWrapper {

	protected transient long id;
	protected transient Stack<Integer> values;
	protected static long nextId;
	protected transient SecureRandom rnd;
	
	static {
		nextId = 1L;
	}
	
	public static void skipId() {
		nextId++;
	}
	
	public RandomWrapper() {
		this.id = nextId++;
		this.rnd = new SecureRandom();
		this.values = new Stack<Integer>();
	}
	
	public long id() {
		return this.id;
	}
	
	public void generate() {
		this.values.push(rnd.nextInt() * ((int) this.id));
	}
	
	public int next() throws NoRandomIntegerGenerated {
		if(this.values.empty()) {
			throw new NoRandomIntegerGenerated();
		} else {
			return this.values.pop();
		}
	}

}
