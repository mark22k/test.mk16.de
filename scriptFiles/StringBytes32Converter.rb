require "erb"

def print_help
  ERB.new(DATA.read).run
end

if ARGV.length != 2
  print_help
  exit
end

input_type = ARGV[0].to_sym

case input_type
when :string
  str = ARGV[1]
  hex = str.unpack("H*")[0]
  if hex.length > 64
    puts "String too long"
    exit
  end
  res = "0x#{hex}#{"0" * (64 - hex.length)}"
  puts "Bytes32: #{res}"
when :bytes32
  str = [ARGV[1][2..-1]].pack("H*")
  puts "String: #{str}"
else
  print_help
end

__END__
Use:
  ruby <%= $PROGRAM_NAME %> [input_type] [data]
  
Input type:
  string           Convert string to bytes32
  bytes32          Convert bytes32 to string
  
Data: Data to be converted
