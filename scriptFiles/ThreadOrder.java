
public class ThreadOrder {

	public static void main(String[] args) {
		Runtime runtime = Runtime.getRuntime();
		int cores = runtime.availableProcessors();
		Thread[] threads = new Thread[cores];
		
		try {
			for (int id = 0; id < cores; id++) {
				Thread tr = new Thread(new Runnable() {
					@Override
					public void run() {
						System.out.println("ID - " + Thread.currentThread().getName());
					}
				});
				tr.setName("thread-" + id);
				System.out.println("Initialize thread " + tr.getName());
				threads[id] = tr;
			}
			
			for(int id = 0; id < cores; id++) {
				System.out.println("Start thread " + threads[id].getName());
				threads[id].start();
			}

			for (int id = 0; id < cores; id++) {
				if (threads[id].isAlive()) {
					System.out.println("Wait for thread " + threads[id].getName());
					threads[id].join();
				}
			}
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
		}
	}

}
