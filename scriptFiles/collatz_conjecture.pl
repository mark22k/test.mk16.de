#!/usr/bin/perl

use strict;
use warnings;

my $startNum;

if ( exists $ARGV[0] ) {
	$startNum = $ARGV[0];
}
else {
	$startNum = <>;
	chomp $startNum;
}

my $workNum = $startNum;
my $counter = 0;

until ( $workNum == 1 ) {
	if ( $workNum % 2 == 0 ) {
		print "$workNum / 2 = ";
		$workNum = $workNum / 2;
		print "$workNum\n";
	}
	else {
		print "$workNum * 3 + 1 = ";
		$workNum = $workNum * 3 + 1;
		print "$workNum\n";
	}
	$counter++;
}

print "The program was able to reach the number $workNum in $counter "
  . ( $counter == 1 ? "round" : "rounds" ) . "!\n";
