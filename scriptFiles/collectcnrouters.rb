=begin
Copyright 2020 Marek Küthe

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.


This Ruby Script stores all Chinese IP addresses of I2P routers in a YAML file.
The first argument specifies the file name. The second argument is optimal and
specifies the I2P router console port.
=end

require "yaml"
require "net/http"

if ARGV.length == 0
  puts "filename?"
  exit
elsif ARGV.length >= 1
  $filename = ARGV[0]
  $filename += ".yaml" if (not $filename.end_with? ".yaml") and (not $filename.end_with? ".yml")
end

if ARGV.length >= 2
  $port = ARGV[1]
else
  $port = "7657"
end

par = Net::HTTP.get(URI("http://127.0.0.1:#{$port}/netdb?c=cn"))

ips = par.scan(/\Host:<\/span> <span class="(?:.*)">((?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])))<\/span\>/)

ips.map! { |o| o[0] }
ips.uniq!
ips.sort!


File.write $filename, YAML.dump(ips)
