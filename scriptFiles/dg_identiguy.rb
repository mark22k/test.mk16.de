require "json"
require "rmagick"
require "gruff"
require "date"

def drawGraph counts, dates
  rd = Hash.new
  i = 0
  (dates.map { |e| e.strftime("%m/%d") }).each { |d|
    rd[i] = d
    i += 1
  }
  graph = Gruff::Line.new "900x600"  # what is a good size? email me if you has a idea: m.k@mk16.de
  graph.line_width = 4
  graph.dot_radius = 4
  graph.title = "Eepsites"
  graph.hide_lines = false
  graph.labels = rd
  graph.theme = {
    :colors => ['#3B5998'],
    :marker_color => 'silver',
    :font_color => 'black',
    :background_colors => ['white', 'silver']
  }
  graph.data("Number of active eepsites", counts.collect)
 
  dates.sort! 
  graph.write "#{dates[0].strftime("%Y-%m-%d")}_#{dates[-1].strftime("%Y-%m-%d")}.png"
end

data = Hash.new
i = 0

Dir["dat_*.json"].each { |fn|
  tmp = JSON.load(File.read fn)[0]
  
  fn[4 + 13] = ":"
  fn[4 + 16] = ":"
  data[DateTime.parse(fn)] = tmp
  
  puts "#{fn}, "
}

data = data.sort.to_h
datas = data.keys
counts = data.values
len = nil

if ARGV.length != 0
    len = ARGV[0].to_i
else
   len = 30
end

len -= 1

for i in 0...(data.length / len)
    i = i * len
    puts "drawGraph counts[#{i}..#{i+len}], datas[#{i}..#{i+len}]"
    drawGraph counts[i..i+len], datas[i..i+len]
end