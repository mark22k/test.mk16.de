# import all gpg keys in the folder key-cert via gpg --import

class RegistryParser

    def initialize data
        @data = data
        @para = {}
    end
    
    def [] para
        return @para[para]
    end
    
    def parse
        @data.each_line { |line|
            begin
            para_raw = line[0...20]
            para = para_raw.strip.chop
            value = line[20..-1].chomp
            @para[para] = [] unless @para[para]
            @para[para] << value
            rescue
                puts "Error: #{$!}"
            end
        }
    end

end


Dir["key-cert/PGPKEY-*"].each { |file|
    puts "Reading file #{file}..."
    
    parser = RegistryParser.new File.read(file)
    parser.parse
    id = parser["key-cert"][0]
    puts "Parsing #{id} from `#{parser["owner"][0]}`"
    key = parser["certif"].join("\n")
    puts "Saving cert..."
    File.write "#{id}.asc", key
    puts "Importing..."
    system "gpg --import #{id}.asc 2> /dev/null"
}
