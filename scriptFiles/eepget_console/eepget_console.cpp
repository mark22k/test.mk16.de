// Copyright 2019 Marek Küthe

/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veroeffentlichten Version, weiter verteilen und/oder modifizieren.

    Dieses Programm wird in der Hoffnung bereitgestellt, dass es nuetzlich sein wird, jedoch
    OHNE JEDE GEWAEHR,; sogar ohne die implizite
    Gewaehr der MARKTFAEHIGKEIT oder EIGNUNG FUER EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License fuer weitere Einzelheiten.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
	
*/

#include <iostream>
#include <string>
#include <stdexcept>
#include <exception>
#include <fstream>

#include <cstdlib>

#include "i2psam.h"

int main()
{
    try
    {
        std::cout << "Hello!" << std::endl << std::endl
                  << "Which program name should be sent "
                     "to the I2P router to set up the tunnel? " << std::flush;
        std::string streamname;
        std::getline(std::cin, streamname);
        if(std::cin.fail()) throw std::runtime_error("Invalid input!");
        
        std::cout << "Establish a tunnel ..." << std::endl;
        SAM::StreamSession s(streamname);
        
        std::string option;
        do
        {
            std::cout << "What should be done? (lookup [eepsite], get, example, exit)  " << std::flush;
            std::getline(std::cin, option);
            if(std::cin.fail()) throw std::runtime_error("Invalid input!");
            
            if(option.substr(0, 7) == "example")
            {
                std::cout << "Hello!" << std::endl <<
                std::endl <<
                "Which program name should be sent to the I2P router to set up the tunnel? eepget" << std::endl <<
                "Establish a tunnel ..." << std::endl <<
                "What should be done? (lookup [eepsite], get, example, exit)  lookup secure.thetinhat.i2p" << std::endl <<
                "Result: 0ncSrtVS20zwfcM7h2S6SSF56uVM2bftQwf40jsWKASNQnzyDVEzXpS04y-DJpm9EwKMGkgvx8ICBX-80W4E9xPJEdGFbb2u34fWmpTVMc3vwwB9ywmSXoxFbwiFx2sm7-HCcdALZwrjU3J41AfBvpEVkB5dXklTZIh~bU0JBTK2JIvQMD0XrSOztEruTc5kYymtkiCUpJaJJFXyIM3lKRcNlZ76UidE8AyQxHX7s9OR02pk7FhYV8Uh-Bs8loAZg6IPZzoYnnBYyi--b1-N8Ipv3aKmqSZPbQEzfQxU8-BE74xBLNEWAJtB8ptKMiKfHphO7qDKWqTzOU-7BtGXZAEOA3oblRAQcgqUbi~aICj0V0MAuYAdj7f-8BIi2k3Qfcl6k6XOFEpZqYFle71LeCjIZN~0mDDzxlr0Scx6LKMGnQAtYlGXFq99urp1MutPDZEu47mdxGWqc9CoNNNsE2UgS9ykvWygefNpZhkmceBXmDxWhuAPD1M2~eNF-fCMBQAIAAMAADZv~vU=" << std::endl <<
                "What should be done? (lookup [eepsite], get, example, exit)  get" << std::endl <<
                "base64(result of lookup): 0ncSrtVS20zwfcM7h2S6SSF56uVM2bftQwf40jsWKASNQnzyDVEzXpS04y-DJpm9EwKMGkgvx8ICBX-80W4E9xPJEdGFbb2u34fWmpTVMc3vwwB9ywmSXoxFbwiFx2sm7-HCcdALZwrjU3J41AfBvpEVkB5dXklTZIh~bU0JBTK2JIvQMD0XrSOztEruTc5kYymtkiCUpJaJJFXyIM3lKRcNlZ76UidE8AyQxHX7s9OR02pk7FhYV8Uh-Bs8loAZg6IPZzoYnnBYyi--b1-N8Ipv3aKmqSZPbQEzfQxU8-BE74xBLNEWAJtB8ptKMiKfHphO7qDKWqTzOU-7BtGXZAEOA3oblRAQcgqUbi~aICj0V0MAuYAdj7f-8BIi2k3Qfcl6k6XOFEpZqYFle71LeCjIZN~0mDDzxlr0Scx6LKMGnQAtYlGXFq99urp1MutPDZEu47mdxGWqc9CoNNNsE2UgS9ykvWygefNpZhkmceBXmDxWhuAPD1M2~eNF-fCMBQAIAAMAADZv~vU=" << std::endl <<
                "HTTP Header (filename): /home/testuser/httpheader" << std::endl <<
                "Result (filename): /home/testuser/result" << std::endl <<
                "Write ... GET / HTTP/1.1" << std::endl <<
                "Reading ..." << std::endl <<
                "Complete!" << std::endl <<
                "What should be done? (lookup [eepsite], get, example, exit)  exit" << std::endl <<
                "" << std::endl <<
                "/home/testuser/httpheader:" << std::endl <<
                "GET / HTTP/1.1" << std::endl <<
                "Host: secure.thetinhat.i2p" << std::endl <<
                std::endl <<
                "The [httpheader] file contains the HTTP header which should be sent to the server." << std::endl <<
                "In this case it is only two lines. The characters \"\\r\\n\" are then sent to the" << std::endl <<
                "server without user intervention. After each line the characters \"\\r\\n\" are sent," << std::endl <<
                "therefore the characters \"\\r\\n\" are sent at the end of the HTTP header." << std::endl <<
                "In the file [resfile] the answer of the server is stored. At best, this is the" << std::endl <<
                "Eepsite. In a less good case, for example, this is an error message. The response" << std::endl <<
                "HTTP header is also stored in the file. Eepsites such. B. stats.i2p or zzz.i2p" << std::endl <<
                "can return an error message because they want to protect themselves from spam and" << std::endl <<
                "DOS." << std::endl << std::endl;
            }
            else if(option.substr(0, 6) == "lookup")
            {
                std::string eepsite = option.substr(7, option.length() - 7);
                auto res = s.namingLookup(eepsite);
                if(res.isOk)
                    std::cout << "Result: " << res.value << std::endl;
                else
                    std::cout << "Error" << std::endl;
            }
            else if(option.substr(0, 3) == "get")
            {
                std::string base64;
                std::string filtmp;
                std::string resfilename;
                
                std::cout << "base64(result of lookup): " << std::flush;
                std::getline(std::cin, base64);
                
                std::cout << "HTTP Header (filename): " << std::flush;
                std::getline(std::cin, filtmp);
                
                std::cout << "Result (filename): " << std::flush;
                std::getline(std::cin, resfilename);
                
                std::ifstream httpheader(filtmp);
                if(! httpheader.is_open()) throw std::runtime_error("Cann't open file httpheader");
                
                auto connRes = s.connect(base64, false);
                if(! connRes.isOk) throw std::runtime_error("Error: Connection Result");
                
                auto conn = connRes.value.get();
                
                while( std::getline(httpheader, filtmp) )
                {
                    std::cout << "Write ... " << filtmp << std::endl;
                    conn->write(filtmp);
                    conn->write("\r\n");
                }
                conn->write("\r\n");
                
                std::cout << "Reading ..." << std::endl;
                
                std::ofstream resfile(resfilename, std::ios::in | std::ios::trunc);
                if(! resfile.is_open()) throw std::runtime_error("Cann't open file resfile");
                
                auto reply = conn->read();
                
                while(! reply.empty() )
                {
                    resfile << reply;
                    if(resfile.fail()) throw std::runtime_error("Cann't write to resfile");
                    reply = conn->read();
                }
                
                std::cout << "Complete!" << std::endl;
                
                resfile.close();
                httpheader.close();
                conn->close();
            }
        }
        while(option != "exit");
        

    }
    catch(const std::exception & e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        std::clog << "Maybe the problem is with the I2P router configuration." << std::endl <<
                     "The SAM application attachment should be enabled." << std::endl;
        exit(EXIT_FAILURE);
    }
    
    return EXIT_SUCCESS;
}
