require "icap"
require "eth"

addr = ARGV[0]

if ! (addr == addr.downcase) && (! Eth::Utils.valid_address? addr)
  puts "The supplied address failed the checksum test. It might be invalid."
end

puts "Address: #{addr.downcase}"
puts "Address (checksum): #{Eth::Utils.format_address addr}"
puts "ICAP: #{ICAP::calculate_icap ICAP::calculate_bban addr}"
