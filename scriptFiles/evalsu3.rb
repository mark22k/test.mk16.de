require "pp"
require "openssl"

def ReadHex dat
  res = String.new
  dat.to_s.each_char { |chr|
    tmpans = chr.ord.to_s 16
    if tmpans.length == 1
      tmpans = "0#{tmpans}"
    end
    res += tmpans
  }
  res = "0x#{res}"
  return res.to_i 16
end

module SU3RS
  SignatureType = {
    0x0000 => "DSA-SHA1",
    0x0001 => "ECDSA-SHA256-P256",
    0x0002 => "ECDSA-SHA384-P384",
    0x0003 => "ECDSA-SHA512-P521",
    0x0004 => "RSA-SHA256-2048",
    0x0005 => "RSA-SHA384-3072",
    0x0006 => "RSA-SHA512-4096",
    0x0008 => "EdDSA-SHA512-Ed25519ph"
  }
  FileType = {
    0x00 => "zip",
    0x01 => "xml",
    0x02 => "html",
    0x03 => "xml.gz",
    0x04 => "txt.gz"
  }
  ContentType = {
    0x00 => "unknown",
    0x01 => "router update",
    0x02 => "plugin or plugin update",
    0x03 => "reseed data",
    0x04 => "news feed",
    0x05 => "blocklist feed"
  }
end
SU3RS::SignatureType.default = "invalid"
SU3RS::FileType.default = "invalid"
SU3RS::ContentType.default = "invalid"

unless File.exist? ARGV[0]
  puts "Error: #{ARGV[0]} is not a file"
  exit
end

fil = File.new ARGV[0], "rb"
dat = fil.read
fil.close
lens = Hash.new

puts "Magic number: #{dat[0..5]}"
puts "Signature type: #{SU3RS::SignatureType[ReadHex(dat[8..9])]}"
puts "Signature length: #{lens[:siglen] = ReadHex(dat[10..11])}"
puts "Version length: #{lens[:verlen] = dat[13].ord}"
puts "Signer ID length: #{lens[:sigidlen] = dat[15].ord}"
puts "Content length: #{lens[:cntlen] = ReadHex(dat[16..23])}"
puts "File type: #{lens[:filetype] = SU3RS::FileType[ReadHex(dat[25])]}"
puts "Content type: #{SU3RS::ContentType[ReadHex(dat[27])]}"

step1 = lens[:verlen]
if step1 < 16
  step1 = 16
end
step1 += 39
puts "Version: #{dat[40..step1].to_i} (#{Time.at(dat[40..step1].to_i)})"

step2 = step1+lens[:sigidlen]
puts "ID of signer: #{dat[step1+1..step2]}"

step3 = step2+lens[:cntlen]
filename = "content.#{lens[:filetype]}"
File.open(filename, "wb") { |file|
  file.write dat[step2+1..step3]
  file.close
}
puts "Extracted content to #{filename}"

step4 = step3+lens[:siglen]
filename = "signature"
File.open(filename, "wb") { |file|
  file.write dat[step3+1..step4]
  file.close
}
puts "Extracted signature to #{filename}"
# How can I do that? m.k@mk16.de ; mark22k@mail.i2p
#ch = OpenSSL::X509::Certificate.new File.read("mark22k_at_mail.i2p.crt")
#pp ch.public_key.verify_pss("SHA512", dat[step3+1..step4], dat[0..step3], :salt_length => :auto, mgf1_hash: "SHA512")

rest = dat[step4+1..-1].to_s
if rest.length == 0
  puts "Everything was read."
else
  puts "Something was forgotten to read: #{rest.inspect} - #{rest.bytes.join ", "}"
end
