#include <BOB3.h>
#include <stddef.h>

int ucols[] = {OFF, RED, GREEN, BLUE};
// int ucols[] = {OFF, CYAN, AQUAMARINE, BLUE, ROYALBLUE, FORESTGREEN, SEAGREEN, GREEN, YELLOW, ORANGE, KABARED, RED, COLERED, FUCHSIA, PURPLE, UNICORN, VIOLET, WHITE};

void setup()
{

}

void loop()
{
  for(register size_t j = 0; j < sizeof(ucols)/sizeof(*ucols) - 1; j++)
  {  
  	for(register int i = 0, col; i < 17; i++)
  	{
    	col = mixColor(ucols[j], ucols[j + 1], 16 - i, i);
    	bob3.setEyes(col, col);
    	delay(100);
  	}
  }
  
  for(register size_t j = sizeof(ucols)/sizeof(*ucols) - 1; j > 0; j--)
  {  
  	for(register int i = 0, col; i < 17; i++)
  	{
    	col = mixColor(ucols[j], ucols[j - 1], 16 - i, i);
    	bob3.setEyes(col, col);
    	delay(100);
  	}
  }
}