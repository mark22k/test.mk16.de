// Copyright 2019 Marek Küthe

/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veroeffentlichten Version, weiter verteilen und/oder modifizieren.

    Dieses Programm wird in der Hoffnung bereitgestellt, dass es nuetzlich sein wird, jedoch
    OHNE JEDE GEWAEHR,; sogar ohne die implizite
    Gewaehr der MARKTFAEHIGKEIT oder EIGNUNG FUER EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License fuer weitere Einzelheiten.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
	
*/

#include <iostream>
#include <stdexcept>
#include <exception>
#include <string>
#include <regex>
#include <fstream>

#include <cstdio>
#include <cstdlib>

#include "i2psam.h"

#define EEPSITE "identiguy.i2p"

int main(int argc, char * argv[])
{
    try
    {
        
        std::ofstream file;
        bool wfile = false;
        
        if(argc > 1)
        {
            file.open(argv[1]);
            wfile = true;
            if(! file.is_open()) throw std::runtime_error("error ofstream 1 - Can not open file!");
        }
        
        std::clog << "Establish a tunnel ..." << std::endl;
    
        SAM::StreamSession s("eepstatus-get");
    
        std::clog << "Naming lookup ..." << std::endl;
        auto lookup = s.namingLookup(EEPSITE);
        if(! lookup.isOk) throw std::runtime_error("Error namingLookup");
    
        std::clog << "Connect to " EEPSITE " ..." << std::endl;
        auto connRes = s.connect(lookup.value, false);
        if(! connRes.isOk) throw std::runtime_error("Error Connection Result");
    
        auto conn = connRes.value.get();
        
        std::clog << "Write request ..." << std::endl;
        conn->write("GET / HTTP/1.1\r\n"
                    "Host: " EEPSITE "\r\n"
                    "\r\n");
        
        std::clog << "Read result ..." << std::endl;
        
        
        std::string tmpfil_fn = tmpnam(NULL);
        std::clog << "Temp file: "      << tmpfil_fn << std::endl;
        std::ofstream tmpfilo(tmpfil_fn);
        if(! tmpfilo.is_open()) throw std::runtime_error("error ofstream 2");
        
        auto reply = conn->read();
        
        while(! reply.empty())
        {
            tmpfilo << reply << "\r\n";
            reply = conn->read();
        }
        
        tmpfilo.close();
        
        std::clog << "Evaluate result ..." << std::endl;
        
        std::ifstream tmpfili(tmpfil_fn);
        if(! tmpfili.is_open()) throw std::runtime_error("error ifstream");
        
        std::regex reg1("<td><a href=\"http:\\/\\/(.*)\\/\\?i2paddresshelper=(.*)\">a<\\/a> <a href=\"http:\\/\\/(.*)\\/\">b<\\/a><\\/td>");
       
        bool res;
        std::smatch m;
        long counter = 0;
        std::string html;
        
        while(std::getline(tmpfili, html))
        {
            res = regex_search(html, m, reg1);
            if(res && m.size() > 1)
            {
                std::cout << m.str(1) << "; " << std::flush;
                
                if(wfile)
                    file << m.str(1) << "  " << m.str(2) << "  " << m.str(3) << "\r\n";

                counter++;
            }
        }
        
        std::cout << std::endl << counter << " eepsites" << std::endl;
        
        
        remove(tmpfil_fn.c_str());
        conn->close();
        file.close();
        tmpfili.close();
        
    }
    catch(const std::exception & e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        exit(EXIT_SUCCESS);
    }
    
    return EXIT_SUCCESS;
}
