﻿Public Class Form1

    Dim NodeID As String
    Dim Tim As Boolean = False
    Dim PU As Integer = 1
    Dim PanalID As Integer

    Sub LoadImage()
        Dim ad As String = "https://stats.ffh.zone/render/dashboard-solo/db/router-fur-meshviewer?var-node=" & NodeID & "&panelId=" & PanalID & "&from=now-12h&to=now-1m&width=510&height=255"
        If PU = 1 Then
            PU = 2
            'MsgBox("1")
            If My.Computer.FileSystem.FileExists("./pic2.png") Then
                My.Computer.FileSystem.DeleteFile("./pic2.png")
            End If
            My.Computer.Network.DownloadFile(ad, "./pic2.png")
            PictureBox1.Load("./pic2.png")
        Else
            PU = 1
            'MsgBox("2")
            If My.Computer.FileSystem.FileExists("./pic1.png") Then
                My.Computer.FileSystem.DeleteFile("./pic1.png")
            End If
            My.Computer.Network.DownloadFile(ad, "./pic1.png")
            PictureBox1.Load("./pic1.png")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Tim = False Then
            NodeID = TextBox1.Text
            PanalID = NumericUpDown2.Value
            NumericUpDown1.Enabled = False
            NumericUpDown2.Enabled = False
            TextBox1.Enabled = False
            Timer1.Interval = NumericUpDown1.Value
            If NodeID = Nothing Then
                MsgBox("Keine ID!", MsgBoxStyle.Critical, "Keine ID!")
                Exit Sub
            End If
            Tim = True
            Button1.Text = "Stop"
            LoadImage()
            Timer1.Start()
        Else
            Timer1.Stop()
            Tim = False
            TextBox1.Enabled = True
            NumericUpDown1.Enabled = True
            NumericUpDown2.Enabled = True
            Button1.Text = "Start"
            PictureBox1.Image = Nothing
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        LoadImage()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        UeberForm.ShowDialog()
    End Sub
End Class
