require "openssl"
require "base64"

=begin
The code at sec2 was written by stesla. It can be viewed at
https://github.com/stesla/base32. It is available under the MIT license.
The code in sec1 has been changed and no longer corresponds to the original code.
The code in sec1 is used to decode the base64.
The code is used to encode the SHA256 sum into a base32.

The rest of the code was written by Marek Küthe and is available under the WTFPL license.
=end

## sec2 start
module Base32
  @table = "abcdefghijklmnopqrstuvwxyz234567".freeze

  class << self
    attr_reader :table
  end

  class Chunk
    def initialize(bytes)
      @bytes = bytes
    end

    def encode
      n = (@bytes.length * 8.0 / 5.0).ceil
      p = n < 8 ? 5 - (@bytes.length * 8) % 5 : 0
      c = @bytes.inject(0) {|m,o| (m << 8) + o} << p
      [(0..n-1).to_a.reverse.collect {|i| Base32.table[(c >> i * 5) & 0x1f].chr},
       ("=" * (8-n))]
    end
  end

  def self.chunks(str, size)
    result = []
    bytes = str.bytes
    while bytes.any? do
      result << Chunk.new(bytes.take(size))
      bytes = bytes.drop(size)
    end
    result
  end

  def self.encode32(str)
    chunks(str, 5).collect(&:encode).flatten.join
  end
end
## sec2 end

if ARGV.length == 0
  $stderr.puts "The base64 should be given as the first argument."
  exit
end

dat = Base64.decode64 ARGV[0].tr "-~", "+/"
if dat.empty?
  $stderr.puts "The specified base64 could not be decoded."
  exit
end

puts Base32.encode32(OpenSSL::Digest::SHA256.digest dat[0...391]).tr("=", "") + ".b32.i2p"