Gem::Specification.new do |spec|
  spec.name = "i2plookuper"
  spec.version = "1.0.2"
  spec.date = "2020-07-06"
  spec.summary = "This gem contains the module I2PLookuper, which in turn includes the classes SAMLookuper and BOBLookuper."
  spec.author = "Marek Küthe"
  spec.email = "m.k@mk16.de"
  spec.files = ["lib/i2plookuper.rb"]
  spec.homepage = "https://mk16.de/"
  spec.license = "WTFPL"
  spec.metadata = {
    "documentation_uri" => "https://www.rubydoc.info/gems/i2plookuper",
    "homepage_uri"      => "https://mk16.de/",
    "source_code_uri"   => "https://test.mk16.de/scriptFiles/i2plookuper/"
  }
end
