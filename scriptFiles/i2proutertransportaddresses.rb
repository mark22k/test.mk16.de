=begin
  Copyright (c) 2020 Marek Küthe
  This program is free software. It comes without any warranty, to
  the extent permitted by applicable law. You can redistribute it
  and/or modify it under the terms of the Do What The Fuck You Want
  To Public License, Version 2, as published by Sam Hocevar. See
  http://www.wtfpl.net/ for more details.
=end

# Router Transport Addresses
class I2PRouterTransportAddresses
  attr_reader :rtastr, :result
  
  def self.getRTAsFromConsole address = "http://127.0.0.1:7657/peers"
    require "net/http"
    return self.new Net::HTTP.get(URI(address)).match(/<pre id="transports">\n((?:.*\n)*)<\/pre>/)[1]
  end
  
  def initialize rtastr
    @rtastr = rtastr.to_s.chomp
  end
  
  def parse!
    ln = 0
    count = 1
    space = Array.new
    r = @rtastr.lines
    @result = Hash.new
    
    while ln < r.length
      str = r[ln]
      if r[ln][0] == "["
        name = "#{str.match(/[^A-Za-z0-9]*([A-Za-z0-9]*)[^A-Za-z0-9]*(\]?)/)[1]}.#{count}"
        count += 1
        space.push name
        @result[name] = Hash.new
        if $3 == "]"
          space.pop
        end
      elsif r[ln] =~ /\t*([A-Za-z0-9]*): ([A-Za-z0-9]*)(\]?)/
        @result[space[0]][$1] = $2
        if $3 == "]"
          space.pop
        end
      elsif r[ln] =~ /\t*([A-Za-z0-9]*) \(([0-9]*)\):/
        ln += 1
        sl = r[ln..ln+$2.to_i]
        en = ln+$2.to_i
        space.push $1
        @result[space[0]][space[1]] = Hash.new
        while ln < en
          res = r[ln].match /\t*\[([A-Za-z0-9]*)\] = \[([^\[\]]*)\](\]?)/
          @result[space[0]][space[1]][res[1]] = res[2]
          if $3 == "]"
            space.shift
          end
          ln += 1
        end
        space.pop
        ln -= 1
      elsif r[ln][0] == "]"
        puts "hihi"
        space.pop
      end
      ln += 1
    end
    @valid = space.empty?
    
    return self
  end
  
  def isParseSuccessful?
    @valid
  end
end

rta = I2PRouterTransportAddresses.getRTAsFromConsole.parse!
if rta.isParseSuccessful?
  puts "n - Address:Port (Protocol, Cost)"
  rta.result.each { |n, c|
    hostandport = nil
    if not c["Options"]["host"].nil?
      if c["Options"]["host"].include? ":"
        hostandport = "[#{c["Options"]["host"]}]:#{c["Options"]["port"]}"
      else
        hostandport = "#{c["Options"]["host"]}:#{c["Options"]["port"]}"
      end
    else
      hostandport = "help router"
    end
    puts "#{n[14..-1]} - #{hostandport} (#{c["Type"]}, #{c["Cost"]})"
  }
  
else
  puts "Error"
end
