/*
    Copyright 2020 Marek Kuethe
    This program is free software. It comes without any warranty, to
    the extent permitted by applicable law. You can redistribute it
    and/or modify it under the terms of the Do What The Fuck You Want
    To Public License, Version 2, as published by Sam Hocevar. See
    http://www.wtfpl.net/ for more details.
*/
/*
    Unfortunately there are still errors in the library for the I2P BOB protocol.
    One problem is that boost :: asio does not always read the entire content. If you have any ideas on how to fix the problem, I would be very happy if you contact me.
    Otherwise, I hope you enjoy programming.

    (This library is available under the public domain license WTFPL.)
 */

#include <iostream>
#include <string>
#include <stdexcept>
#include <exception>

#include <cstdlib>

#include "mboblib.hpp"

using namespace std;

int main()
{
    try
    {
        mBOBClient cli;
        
        std::string eepsite;
        cin >> eepsite; /* for example: echelon.i2p */
        if(cin.fail())
            throw std::invalid_argument("Invalid input!");
        
        auto lookup = cli.blookup(eepsite);
        if(lookup.issuccessful())
            cout << "Base64: " << lookup.getcontent() << endl;
        else
            throw std::runtime_error(lookup.getcontent());
    }
    catch(const exception & e)
    {
        cerr << "Error: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }
        
    return EXIT_SUCCESS;
}