/*
    Copyright 2020 Marek Kuethe
    This program is free software. It comes without any warranty, to
    the extent permitted by applicable law. You can redistribute it
    and/or modify it under the terms of the Do What The Fuck You Want
    To Public License, Version 2, as published by Sam Hocevar. See
    http://www.wtfpl.net/ for more details.
*/
#ifndef MBOBLIB_HPP
#define MBOBLIB_HPP

#include "boost/asio.hpp"

#include <new>
#include <string>
#include <stdexcept>
#include <cstddef>

class mBOBResult
{
    protected:
        std::string status;
        std::string content;
        std::string data;
        
    public:
        mBOBResult(std::string);
        mBOBResult(std::string, std::string);
        bool issuccessful();
        std::string getstatus();
        std::string getcontent();
        std::string getdata();
        
    friend class mBOBClient;
};

class mBOBClient
{
    protected:
        std::shared_ptr<boost::asio::io_service> ios;
        std::shared_ptr<boost::asio::ip::tcp::endpoint> ep;
        std::shared_ptr<boost::asio::ip::tcp::socket> sock;
        std::string host;
        std::string bobversion;
        unsigned short port;
        
        std::string readsocket(size_t);
        void writesocket(std::string);
        
    public:
        mBOBClient(std::string = "127.0.0.1", unsigned short = 2827);
        ~mBOBClient();
        std::string getbobversion();
        mBOBResult bhelp(std::string = "");
        mBOBResult bclear();
        mBOBResult bgetdest();
        mBOBResult bgetkeys();
        mBOBResult bgetnick();
        mBOBResult binhost(std::string);
        mBOBResult binport(std::string);
        mBOBResult blist();
        mBOBResult blookup(std::string);
        mBOBResult bnewkeys();
        mBOBResult boption(std::string);
        mBOBResult bouthost(std::string);
        mBOBResult boutport(std::string);
        mBOBResult bquiet(bool);
        mBOBResult bsetkeys(std::string);
        mBOBResult bsetnick(std::string);
        mBOBResult bshow();
        mBOBResult bshowprops();
        mBOBResult bstart();
        mBOBResult bstatus(std::string);
        mBOBResult bstop();
        mBOBResult bverify(std::string);
        mBOBResult bvisit();
        mBOBResult bzap();
        void readmore(mBOBResult *);
        bool close();
        std::string direct(std::string, size_t = 5);
};

#endif
