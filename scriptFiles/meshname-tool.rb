
require "ipaddr"
require "meshname"

def print_help
  puts
  puts "Create domain: ruby #{__FILE__} create_domain IPADDR"
  puts "Resolv domain: ruby #{__FILE__} resolv_domain DOMAIN"
end

if ! ARGV[0]
  puts "No argument was given."
  print_help
  exit! false
end

option = ARGV[0].to_sym

case option
when :create_domain
  begin
    ip = IPAddr.new ARGV[1]
  rescue IPAddr::AddressFamilyError => ex
    puts "The address family is invalid such as an address with an unsupported family, an address with an inconsistent family, or an address who's family cannot be determined."
    puts "Error: #{ex}"
    exit! false
  rescue IPAddr::InvalidAddressError => ex
    puts "The provided IP address is an invalid address."
    puts "Error: #{ex}"
    exit! false
  rescue IPAddr::InvalidPrefixError => ex
    puts "The address is an invalid length"
    puts "Error: #{ex}"
    exit! false
  rescue IPAddr::Error => ex
    puts "Error: #{ex}"
    exit! false
  end

  if ! ip.ipv6?
    puts "Argument must be an ipv6 address"
    exit! false
  end

  mn = Meshname::getname ip

  puts "#{mn}.meship\tpoints directly to the IP\t#{ip.to_s}"
  puts "#{mn}.meshname\tpoints to the DNS server\t#{ip.to_s}"

when :resolv_domain
  if ! ARGV[1].end_with?(".meship") && ! ARGV[1].end_with?(".meshname")
    puts "Domain must end with .meship or .meshname"
    exit! false
  elsif ARGV[1][26] != "."
    puts "Domain has an invalid length"
    exit! false
  end
  
  res = Meshname::resolv(ARGV[1])
  if res.length == 0
    puts "No IP address could be found for the domain. In the case of a .meshname domain, the DNS server may be down."
  else
    res.each { |record|
      puts "IP: #{record.to_s}"
    }
  end
else
  puts "The specified option does not exist."
  print_help
end