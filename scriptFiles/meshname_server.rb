
require "meshname"
require "resolv"
require "async/dns"

def parse_address str
  case str
  when /\A\[(?<address> .* )\]:(?<port> \d+ )\z/x
    address, port = $~[:address], $~[:port]
  when /\A(?<address> [^:]+ ):(?<port> \d+ )\z/x
    address, port = $~[:address], $~[:port]
  else
    address, port = str, nil
  end
  
  return [address, port]
  # source: https://rosettacode.org/wiki/Parse_an_IP_Address
end

addr = ARGV[0] ? ARGV[0] : "[::1]:53535"
parsed_addr = parse_address addr

class MeshnameDnsServer < Async::DNS::Server
  def process name, resource_class, tx
    if name =~ /[abcdefghijklmnopqrstuvwxyz234567]{26}.(meshname|meship)/ && resource_class == Resolv::DNS::Resource::IN::AAAA
      Meshname.resolv(name).map! { |ip|
        tx.respond! ip.to_s
      }
    end
  end
end

srv = MeshnameDnsServer.new [[:udp, parsed_addr[0], parsed_addr[1].to_i]]
srv.run
