// Copyright 2019 Marek Kuethe

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Als erstes wird der Bob3 Pseudo-Gebootet:
    Die Augen beginnen Weisz zu leuchten.
    Danach werden die Augen beim PC angeschlossen PURPLE leuchten,
    sonst BLUE.
    Das bedeutet das man nun in der loop-Funktion ist und
    man sich entscheiden kann:
    drueckt man den linken Arm(1), sendet
    der Bob3 mittels Morsecodes das Wort
    "Hello World". Drueckt man den rechten Arm(2)
    sendet er das Wort "SOS".
    Nach jeden Uebertragenen Buchstaben, wechselt die
    Augenfarb von Orange auf OFF und andersum.
    Am Ende sind beide Augen fuer eine Weile Gruen.
    Danach kann man sich wieder entscheiden.
*/

/* Dieses Programm wurde fuer den Bob3 entwickelt. */

#include <BOB3.h>
#include <stddef.h> /* size_t */

#define MORSE_BREAK 1000
#define MORSE_LONG 500
#define MORSE_SHORT 250
#define MORSE_LETT 100

#define mb { delay(MORSE_BREAK); }
#define ml { bob3.setWhiteLeds(ON, ON); delay(MORSE_LONG); bob3.setWhiteLeds(OFF, OFF); delay(MORSE_LETT); }
#define ms { bob3.setWhiteLeds(ON, ON); delay(MORSE_SHORT); bob3.setWhiteLeds(OFF, OFF); delay(MORSE_LETT); }

bool e;

inline void sendMessage(char * str, size_t len)
{
    for(size_t i = 0; i < len; i++)
    {
        bob3.setEyes((e?ORANGE:OFF), (e?OFF:ORANGE));
        e = !e;
        switch(str[i])
        {
            /*case 'A': ms ml break;
            case 'B': ml ms ms ms break;
            case 'C': ml ms ml ms break;
            */case 'D': ml ms ms break;/*
            */case 'E': ms break;/*
            case 'F': ms ms ml ms break;
            case 'G': ml ml ms break;
            */case 'H': ms ms ms ms break;/*
            case 'I': ms ms break;
            case 'J': ms ml ml ml break;
            case 'K': ml ms ml break;
            */case 'L': ms ml ms ms break;/*
            case 'M': ml ml break;
            case 'N': ml ms break;
            */case 'O': ml ml ml break;/*
            case 'P': ms ml ml ms break;
            case 'Q': ml ml ms ml break;
            */case 'R': ms ml ms break;/*
            */case 'S': ms ms ms break;/*
            case 'T': ml break;
            case 'U': ms ms ml break;
            case 'V': ms ms ms ml break;
            */case 'W': ms ml ml break;/*
            case 'X': ml ms ms ml break;
            case 'Y': ml ms ml ml break;
            case 'Z': ml ml ms ms break;*/
            case ' ': mb break;
            case '\0':
                bob3.setEyes(UNICORN, UNICORN);
                delay(500);
                bob3.setEyes(OFF, OFF);
            default: bob3.setEyes(RED, RED); break;
        }
    }
    bob3.setEyes(GREEN, GREEN);
}

void setup()
{
    /* init */
    e = 1;
    
    /* welcome setup */
    short col;
    for(short i = 0; i < 256; i++)
    {
        col = mixColor(OFF, WHITE, 255 - i, i);
        bob3.setEyes(col, col);
        delay(5);
    }
    delay(100);
}

void loop()
{
    if(bob3.getArm(1) != 0)
    {
        /* when you touch arm 1 */
    	/* send/morse hello world */
    	char str[] = "HELLO WORLD";
    	sendMessage(str, sizeof(str)/sizeof(*str)); 
        delay(2000);
    }
    else if(bob3.getArm(2) != 0)
    {
        /* when you touch arm 2 */
        /* send/morse SOS */
    	char str[] = "SOS";
    	sendMessage(str, sizeof(str)/sizeof(*str));
        delay(2000);
    }
    else
    {
        if(bob3.getMillivolt() > 3000)
        {
            /* when bob3 connect to a pc */
            bob3.setEyes(PURPLE, PURPLE);
        }
        else
        {
            bob3.setEyes(BLUE, BLUE);
        }
    }
    delay(20);
}
