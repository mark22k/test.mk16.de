// Copyright 2019 Marek Kuethe

/*
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/*

    Problem: https://projecteuler.net/problem=2

    GCC compile command:
        g++ mproblem2.cpp -o mproblem2.out -O3 
  
*/

#include <iostream>

#include <cstdlib>

#define WERT 4'000'000

using namespace std;

int main()
{
    int v1 = 0,
        v2 = 1;
    int temp = 1;
    int sum = 0;
    
    cout << "0, ";
    
    do
    {
        
        if(temp % 2 == 0)
        {
            cout << temp << ", ";
            sum += temp;
        }
        temp = v1 + v2;
        v1 = v2;
        v2 = temp;
        
    }
    while(temp < WERT);
    
    cout << "\b\b = " << sum << endl;
    
    return EXIT_SUCCESS;
}
