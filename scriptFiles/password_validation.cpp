// Author: Marek K.

/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veroeffentlichten Version, weiter verteilen und/oder modifizieren.

    Dieses Programm wird in der Hoffnung bereitgestellt, dass es nuetzlich sein wird, jedoch
    OHNE JEDE GEWAEHR,; sogar ohne die implizite
    Gewaehr der MARKTFAEHIGKEIT oder EIGNUNG FUER EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License fuer weitere Einzelheiten.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
	
*/

#include <iostream>
#include <string>
#include <exception>
#include <stdexcept>

#include <cstdlib>
#include <cctype>

using namespace std;

/*
    password_validation from sololearn code coach
    https://www.sololearn.com/coach/76
*/

bool validate(string pw)
{
    if(pw.length() < 7)
        return false;
    
    short n = 0,
          s = 0;
    for(unsigned int i = 0; i < pw.length(); i++)
    {
        if(isdigit(pw[i]))
            n++;
        if(pw[i] == '!' ||
           pw[i] == '@' ||
           pw[i] == '#' ||
           pw[i] == '$' ||
           pw[i] == '%' ||
           pw[i] == '&' ||
           pw[i] == '*')
            s++;
    }
    
    if(n < 2 || s < 2)
        return false;
    
    return true;
}

int main()
{
    
    try
    {
        string pw;
        
        cout << "Password: " << flush;
        getline(cin, pw);
        
        if(validate(pw))
            cout << "Strong" << endl;
        else
            cout << "Weak" << endl;
    }
    catch(const exception & e)
    {
        cerr << "Error: " << e.what();
    }
    
    return EXIT_SUCCESS;
}
