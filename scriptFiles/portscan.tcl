set from 0
set to 65536
set host 127.0.0.1
set outputClosed false

if { $argc > 4 } {
    error "Too much arguments"
}

if { $argc >= 1 } {
    set arg1 [lindex $argv 0]
    if { $arg1 == "true" ||
         $arg1 == "." } {
        set outputClosed true
    } elseif { $arg1 == "false" ||
         $arg1 == "," } {
        set outputClosed false
    } elseif { $arg1 == "help" } {
        puts "Usage: $tcl_platform(engine) - [info nameofexecutable] portscan.tcl \[output closed / help\] \[from port\] \[to port\]"
        exit
    } else {
        error "Use true or false"
    }
}

if { $argc >= 2 } {
    set from [scan [lindex $argv 1] %d]
}

if { $argc >= 3 } {
    set to [scan [lindex $argv 2] %d]
}

if { $argc >= 4 } {
    set host [lindex $argv 3]
}

puts "Scan all ports from $from to $to on the host $host ..."

for {set port $from} { $port < $to } {incr port} {
    try {
        close [socket $host $port]
    } trap {POSIX ECONNREFUSED} { } {
        if { $outputClosed } {
            puts "Port closed: $port"
        }
    } on ok { rs } {
        puts "Port open: $port"
        if { $rs != "" } {
            puts "Status: *$rs*"
        }
    }
}
