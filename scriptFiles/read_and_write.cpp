// Copyright 2019 Marek Küthe

/*

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veroeffentlichten Version, weiter verteilen und/oder modifizieren.

    Dieses Programm wird in der Hoffnung bereitgestellt, dass es nuetzlich sein wird, jedoch
    OHNE JEDE GEWAEHR,; sogar ohne die implizite
    Gewaehr der MARKTFAEHIGKEIT oder EIGNUNG FUER EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License fuer weitere Einzelheiten.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
	
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdexcept>
#include <exception>
#include <string>

#include <cstddef>
#include <cstdlib>

/*
 Example: ./read_and_write out 8192 8192 /dev/urandom
   or
          ./read_and_write out 8192 8192 /dev/ttyACM0
    ttyACM0: https://shop.fsf.org/storage-devices/neug-usb-true-random-number-generator
*/

using namespace std;

int main(int argc, char * argv[])
{
    try
    {
        if(argc < 4)
        {
            throw runtime_error("program filename count1 count2 stream");
        }
        /* count = count1 * count2 * 255 */
        
        clog << "Read arguments ..." << endl;
        
        string filename(argv[1]);
        size_t count = atoi(argv[2]) * atoi(argv[3]);
        size_t digilen = to_string(count).length();
        string streamname(argv[4]);
        
        string del;
        for(size_t i = 0; i < 2 + 4 + 1 + 2 * digilen; i++)
            del += "\b";
        
        clog << "Open file streams ..." << endl;
        ifstream stream(streamname, ios::in | ios::binary);
        ofstream file(filename, ios::out | ios::trunc);
        
        if(! stream.is_open())
            throw runtime_error("Can not open /dev/ttyACM0 ; Try to run this program as sudo");
        
        if(! file.is_open())
            throw runtime_error("Can not open file");
        
        unsigned char rv[255];
        double lp = 0, ap = 0;
        /* register */size_t rvs = sizeof(rv) / sizeof(*rv);
        /* register */size_t size = sizeof(*rv) * rvs;
        
        clog << "Read and write random data ..." << endl;
        
        for(size_t i = 0; i < count; i++)
        {
            stream.read(reinterpret_cast<char *>(&rv), size);
        
            for(size_t i = 0; i < rvs; i++)
            {
                file << rv[i];
                //cout << static_cast<short>(rv[i]) << " - ";
            }
            
            ap = static_cast<short>(static_cast<double>(i) / static_cast<double>(count) * static_cast<double>(100.0));
            if(ap != lp)
                //cout << setfill('0') << setw(2) << ap << "% - " << setfill('0') << setw(digilen) << i << "/" << count << endl;
                clog << del << setfill('0') << setw(2) << ap << "% - " << setfill('0') << setw(digilen) << i << "/" << count << flush;
            lp = ap;
        }
        
        clog << del << "100% - " << count << "/" << count << endl <<
                "Written " << count * 255 << " bytes." << endl <<
                "Close streams ..." << endl;
        file.close();
        stream.close();
    }
    catch(const exception & e)
    {
        cerr << "Error: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }
    
    return EXIT_SUCCESS;
}

