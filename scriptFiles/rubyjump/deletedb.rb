require "gdbm"

require_relative "config.rb"

db = GDBM.new $gdbm_file
db.clear
db.close