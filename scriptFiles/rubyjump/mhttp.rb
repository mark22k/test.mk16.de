require "erb"

module MHTTP
  class ServResponse
    def self.fromSocket sock
      data = String.new
      
      while (ans = sock.gets).chop != ""
        data += ans
      end
      
      return ServResponse.new data
    end
    
    def initialize answer
      @ans = answer
      @meta = Hash.new
      @head = Hash.new
    end
    
    def parse!
      lins = @ans.lines
      
      fw = lins[0].index " "
      @meta[:method] = lins[0][0...fw].chomp
      lw = lins[0].index " ", fw + 1
      @meta[:path] = lins[0][fw + 1...lw].chomp
      @meta[:protocol] = lins[0][lw + 1..-1].chomp
      
      i = 1
      while i < lins.length
        fw = lins[i].index ":"
        @head[lins[i][0...fw]] = lins[i][fw+2..-1].chomp
        i += 1
      end
      
      i += 1 while lins[i] == "\n"
      lins.shift i
      @body = lins.join.chomp
      return self
    end
    
    def meta
      return @meta
    end
    
    def head
      return @head
    end
    
    def body
      return @body
    end
  end
  
  class ServRequest
    def self.fromResponse resp, statcode, statmsg, head, body
      return ServRequest.new resp.meta[:protocol], statcode, statmsg, head, body
    end
    
    def initialize protocol, statuscode, statusmsg, head, body
      @meta = Hash.new
      @meta[:protocol] = protocol
      @meta[:statuscode] = statuscode
      @meta[:statusmsg] = statusmsg
      @head = head
      @body = body
    end
    
    def prepare!
      @res = ERB.new(File.read("./templates/httpresponse.erb")).result binding
      return self
    end
    
    def request
      return @res
    end
  end
end