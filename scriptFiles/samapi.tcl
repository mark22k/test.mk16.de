
#  Copyright (c) 2020 Marek Küthe
#  This program is free software. It comes without any warranty, to
#  the extent permitted by applicable law. You can redistribute it
#  and/or modify it under the terms of the Do What The Fuck You Want
#  To Public License, Version 2, as published by Sam Hocevar. See
#  http://www.wtfpl.net/ for more details.

namespace eval samapi {
	
	# default sam settings
	variable defaultHost 127.0.0.1
	variable defaultPort 7656
	
	# example: samapi::create_samcmd HELLO VERSION
	# example: samapi::create_samcmd NAMING LOOKUP name bandura.i2p
	proc create_samcmd {fircmd {seccmd ""} args} {
		set cmd [string toupper $fircmd]
		
		if {$seccmd != ""} {
			append cmd " " [string toupper $seccmd]
		}
		
		foreach {key value} $args {
			append cmd " [string toupper $key]=\"$value\""
		}
		
		return $cmd
	}
	
	# return a list: [main command, second command, key1, val1, key2, val2, keyn, valn]
	proc parse_samcmd {cmd} {
		set cmdlen [string length $cmd]
		
		set firpos [string first " " $cmd]
		set fircmd [string range $cmd 0 [expr $firpos - 1]]
		set secpos [string first " " $cmd [expr $firpos + 1]]
		if {$secpos == -1} {
			# case that no arguments are given -
			#     as a result only one space exists
			set secpos $cmdlen
		}
		set seccmd [string range $cmd [expr $firpos + 1] [expr $secpos - 1]]
		
		set argus [list $fircmd $seccmd]
		
		for {set pos [expr $secpos + 2]} {$pos != [expr $cmdlen + 2]} {set pos [expr $nextpos + 2]} {
			
			set equelsign [string first "=" $cmd $pos]
			set signAfterEs [string index $cmd [expr $equelsign + 1]]
			if {$signAfterEs == "\""} {
				# case: argument is in "
				set nextpos [expr [string first "\"" $cmd [expr $equelsign + 2]] + 1]
			} else {
				# case: argument is not in "
				set nextpos [string first " " $cmd $pos]
			}
			
			if {$nextpos == -1} {
				set nextpos $cmdlen
			}
			
			lappend argus [string range $cmd [expr $pos - 1] [expr $equelsign - 1]] [string trim [string range $cmd [expr $equelsign + 1] [expr $nextpos - 1]] "\""]
		
		}
		
		return $argus
	}
}
