require "csv"

if ! File.readable ARGV[0]
  puts "I cannot read the file"
end

file = CSV.open(ARGV[0], "r") 
data = file.read

crypto_column = 10
fiat_column = 11

crypto = data[1..-1].inject([]) { |arr, line| arr << line[crypto_column].to_f }
fiat = data[1..-1].inject([]) { |arr, line| arr << line[fiat_column].to_f }

def print_stats title, arr
  puts "#{title}"
  puts "\tCount: #{arr.length}"
  puts "\tSum: #{arr.sum}"
  puts "\tMin: #{arr.min}"
  puts "\tMax: #{arr.max}"
  puts "\tAvg: #{arr.sum / arr.length.to_f}"
  puts
end

print_stats data[0][crypto_column], crypto
print_stats data[0][fiat_column], fiat
