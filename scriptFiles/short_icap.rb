
require "eth"
require "icap"

number_of_threads = nil
if ARGV[0]
  number_of_threads = ARGV[0].to_i
else
  require "etc"
  number_of_threads = Etc.nprocessors
end

def output_key key
  puts "addr: #{key.address}"
  puts "priv: #{key.private_hex}"
  puts "icap: #{ICAP::calculate_icap ICAP::calculate_bban key.address}"
  puts "-" * 60
end

max = 0
max_mutex = Mutex.new

threads = []
for i in 0..number_of_threads
  puts "Start thread (#{i}/#{number_of_threads})..."
  threads << Thread.new {
    loop {
      key = Eth::Key.new
      nos = key.address[2..-1].index(/[^0]/)
      if nos > max
        max_mutex.synchronize {
          max = nos
        }
        output_key key
      end
    }
  }
end

threads.each { |th|
  th.join
  puts "Exit thread..."
}
