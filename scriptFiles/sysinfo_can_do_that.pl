#!/usr/bin/perl

use strict;
use warnings;

use Sys::Info;

my $info = Sys::Info->new;


print "Perl version:\n";
print "  - $]\n";
printf "  - %s\n", $info->perl;
printf "Build number: %s\n", $info->perl_build if $info->perl_build;
printf "HTTP server: %s\n", $info->httpd if $info->httpd;
print "\n";

my $os = $info->os();
my %fs = $os->fs;

print "Operating system:\n";
printf "  - %s\n", $^O;
printf "  - %s\n", $os->name( long => 1 );
printf "OS version: %s\n", $os->version if $os->version;
printf "OS build number: %s\n", $os->build if $os->build;
printf "Machine name: %s\n", $os->node_name if $os->node_name;
printf "Domain name: %s\n", $os->domain_name if $os->domain_name;
printf "Filesystem: %s\n", join(", ", values %fs);
my $user = $os->login_name( real => 1 ) || $os->login_name || undef;
printf "Login name: %s\n", $user if $user;
printf "IP: %s\n", $os->ip if $os->ip;
printf "Hostname: %s\n", $os->host_name if $os->host_name;
printf "Timezone: %s\n", $os->time_zone if $os->time_zone;
printf "Product type: %s\n", $os->product_type if $os->product_type;
printf "Bits: %s\n", $os->bitness if $os->bitness;
my %var = $os->meta;
print "Metadata:\n";
foreach my $name ( keys %var ) {
	printf "  - %s => %s\n", $name, $var{$name} if $var{$name};
}
printf "Locale: %s\n", $os->locale if $os->locale;
printf "Does this user have admin rights? %s\n", $os->is_root ? "True" : "False" if defined $os->is_root;
print "\n";

printf "Devices: %s\n\n", join ", ", $info->device("available");


my $cpu  = $info->device("CPU");

printf "CPU: %s\n", scalar $cpu->identify;
printf "Speed: %s\n", $cpu->speed;
printf "Load: %s\n", $cpu->load if $cpu->load;
printf "Count: %s\n", $cpu->count;
printf "Bits: %s\n", $cpu->bitness;
printf "Does the CPU support Hyper Threading? %s\n", $cpu->hyper_threading ? "True" : "False";
printf "Number of hyper threading threads: %s\n", $cpu->ht if $cpu->ht;

my $i = 0;
foreach my $core ( $cpu->identify ) {
	printf "\nCPU %d:\n", ++$i;
	foreach my $name ( keys %$core ) {
		if(ref($$core{$name}) eq "HASH") {
			my %data = %{ $$core{$name} };
			print "$name:\n";
			foreach my $entry ( keys %data ) {
				printf "    %s => %s\n", $entry, $data{$entry};
			}
		} elsif (ref($$core{$name}) eq "ARRAY") {
			printf "%s => %s\n", $name, join(", ", @{ $$core{$name} });
		} else {
			printf "%s => %s\n", $name, $$core{$name} if $$core{$name};
		}
	}
}
