
puts "\$tcl_libraray\t\t\t= $tcl_library"
puts "\$tcl_patchLevel\t\t\t= $tcl_patchLevel"
puts "\$tcl_version\t\t\t= $tcl_version"
puts "\$tcl_pkgPath\t\t\t= $tcl_pkgPath"
puts "\$tcl_precision\t\t\t= $tcl_precision"
puts "\$tcl_rcFileName\t\t\t= $tcl_rcFileName"
puts "\$tcl_interactive\t\t= $tcl_interactive"
foreach {key val} [array get tcl_platform] {
    if {[string length $key] > 5} {
        puts "\$tcl_platform($key)   \t= $tcl_platform($key)"
    } else {
        puts "\$tcl_platform($key)\t\t= $tcl_platform($key)"
    }
}