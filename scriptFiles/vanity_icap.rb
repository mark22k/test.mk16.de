
require "eth"
require "icap"

to_find = ARGV[0]
if ! to_find
  puts "What should I search?"
  exit
end

number_of_threads = nil
if ARGV[1]
  number_of_threads = ARGV[1].to_i
else
  require "etc"
  number_of_threads = Etc.nprocessors
end

def output_key key
  puts "addr: #{key.address}"
  puts "priv: #{key.private_hex}"
  puts "icap: #{ICAP::calculate_icap ICAP::calculate_bban key.address}"
  puts "-" * 60
end

threads = []
for i in 0..number_of_threads
  puts "Start thread (#{i}/#{number_of_threads})..."
  threads << Thread.new {
    loop {
      key = Eth::Key.new
      if key.address[2] == "0"
        bban = ICAP::calculate_bban key.address
        if bban.include? to_find
          output_key key
        end
      end
    }
  }
end

threads.each { |th|
  th.join
  puts "Exit thread..."
}
