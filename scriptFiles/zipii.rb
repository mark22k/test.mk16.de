require "zlib"

unless ARGV[0].nil? or File.exist? ARGV[1].to_s
  puts "Error: #{ARGV[1]} not found"
  exit 1
end

if ARGV[0] == "zlib-deflate"
  
  fil = File.open ARGV[1], "rb"
  tar = File.open "#{ARGV[1]}.zlib", "wb"
  tar.puts Zlib::Deflate.deflate fil.read
  puts "#{ARGV[1]} to #{ARGV[1]}.zlib deflated."
  tar.close
  fil.close
elsif ARGV[0] == "zlib-inflate"
  
  fil = File.open ARGV[1], "rb"
  tar = File.open ARGV[1][0..-6], "wb"
  tar.puts Zlib::Inflate.inflate fil.read
  puts "#{ARGV[1]} to #{ARGV[1][0..-6]} inflated."
  tar.close
  fil.close
elsif ARGV[0] == "gzip-deflate"
  Zlib::GzipWriter.open("#{ARGV[1]}.gzip") do |gz|
    fil = File.open ARGV[1], "rb"
    gz.write fil.read
    puts "#{ARGV[1]} to #{ARGV[1]}.gzip deflated."
    fil.close
  end
elsif ARGV[0] == "gzip-inflate"
  Zlib::GzipReader.open(ARGV[1]) do |gz|
    fil = File.open ARGV[1][0..-6], "wb"
    fil.write gz.read
    puts "#{ARGV[1]} to #{ARGV[1][0..-6]} inflated."
    fil.close
  end
else
  puts "Use: #{__FILE__} [zlib|gzip]-[deflate|inflate] [FILE]"
end